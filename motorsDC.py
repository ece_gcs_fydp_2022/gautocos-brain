import RPi.GPIO as GPIO          
from time import sleep

#Connections for RPM40 pins to RaspPi's GPIO ports 
motorRPM40Enable = 21
motorRPM40InputA = 16
motorRPM40InputB = 20

#Connections for RPM20 pins to RaspPi's GPIO ports \
motorRPM20Enable = 6
motorRPM20InputA = 19
motorRPM20InputB = 13

GPIO.setmode(GPIO.BCM)

GPIO.setwarnings(False)

GPIO.setup(motorRPM40InputA,GPIO.OUT)
GPIO.setup(motorRPM40InputB,GPIO.OUT)
GPIO.setup(motorRPM20InputA,GPIO.OUT)
GPIO.setup(motorRPM20InputB,GPIO.OUT)
GPIO.setup(motorRPM40Enable,GPIO.OUT)
GPIO.setup(motorRPM20Enable,GPIO.OUT)

GPIO.output(motorRPM40InputA,GPIO.LOW)
GPIO.output(motorRPM40InputB,GPIO.LOW)
GPIO.output(motorRPM20InputA,GPIO.LOW)
GPIO.output(motorRPM20InputB,GPIO.LOW)

motorRPM40=GPIO.PWM(motorRPM40Enable,1000)
motorRPM20=GPIO.PWM(motorRPM20Enable,1000)

motorRPM40.start(0)
motorRPM20.start(0)

def motor_adjust_speed(motorType, speed):
    if motorType == 'rpm20':
        GPIO.output(motorRPM20InputA,GPIO.HIGH)
        GPIO.output(motorRPM20InputB,GPIO.LOW)
        motorRPM20.ChangeDutyCycle(speed)
    if motorType == 'rpm40':
        GPIO.output(motorRPM40InputA,GPIO.HIGH)
        GPIO.output(motorRPM40InputB,GPIO.LOW)
        motorRPM40.ChangeDutyCycle(speed)
    return

def send_to_second_conveyor_belt():
    motor_adjust_speed('rpm20', 40)
    sleep(18)
    motor_adjust_speed('rpm20', 0)
    return

def send_to_bin(isCompositable):
    if (isCompositable == 1):
        GPIO.output(motorRPM40InputA,GPIO.LOW)
        GPIO.output(motorRPM40InputB,GPIO.HIGH)
    else:
        GPIO.output(motorRPM40InputA,GPIO.HIGH)
        GPIO.output(motorRPM40InputB,GPIO.LOW)
    motorRPM40.ChangeDutyCycle(100)
    sleep(10)
    motorRPM40.ChangeDutyCycle(0)

def motor_OFF(motorType):
    if motorType == 'rpm20':
        GPIO.output(motorRPM20InputA,GPIO.LOW)
        GPIO.output(motorRPM20InputB,GPIO.LOW)
    if motorType == 'rpm40':
        GPIO.output(motorRPM40InputA,GPIO.LOW)
        GPIO.output(motorRPM40InputB,GPIO.LOW)
    return

