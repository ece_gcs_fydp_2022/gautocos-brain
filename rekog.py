import boto3


def detect_labels_local_file(photo):

    client = boto3.client('rekognition')

    with open(photo, 'rb') as image:
        response = client.detect_labels(Image={'Bytes': image.read()})

    print('Detected labels in ' + photo)
    return response['Labels']


def predict(file_path):

    compost_set = {
        "Food", "Fruit", "Plant", "Flower", "Banana", "Peel", "Bread",
        "Dessert", "Cookie", "Biscuit"
    }
    labels = detect_labels_local_file(file_path)
    top = labels[0]
    print("Top label/confidence:", top["Name"], top["Confidence"])

    if top["Confidence"] > 90:
        if top["Parents"]:
            for parent in top["Parents"]:
                if parent["Name"] in compost_set:
                    return 1
        else:
            if top["Name"] in compost_set:
                return 1
    return 0


if __name__ == "__main__":
    pic = "./pics/cookie.jpeg"
    is_compost = predict(pic)
    if is_compost == 1:
        print("It's compostable")
    else:
        print("Not compost")
