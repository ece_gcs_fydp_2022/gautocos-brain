# Gautocos Brain

Python micro-controller code responsible for coordinating Gautocos operations.

Please read the comments in general-logic.py

# Circuit Connection:
    Pin Usage:

    DC Motor 12V 40RPM
        L298N’s ENA = pin #40 
        L298N’s IN1 = pin #38
        L298N’s IN2 = pin #36

    DC Motor 12V 20RPM
        L298N’s IN3 = pin #35 
        L298N’s IN4 = pin #33 
        L298N’s ENB = pin #31

    Ultrasonic Sensor’s
        Vcc = breadboard’s red
        Trig = pin #12 
        Echo = pin #11
        Gnd = breadboard’s blue



# Code Rethrough Below: While-Loop logic until terminates program
    # if no objects are within 30 cm of the ultrasonic sensor 
        # motorRPM20 ON: max-speed
        # motorRPM40 OFF
    # else <- ultrasonic detects an object within 30 cm
        # motorRPM20 OFF
        # Webcam takes picture -> updates resolution -> saves to "pics" directory
        # print: [INFO] logs in Shell -> tracks time from detection to webcam finishes processing photo
        # motorRPM20 ON: max-speed for 18 seconds -> ensures object will reach end of conveyer belt    
          # if isCompostable
              # motorRPM40 ON rotationA: high-speed for 10 seconds -> ensures object reaches end of conveyer belt
          # else <- not Compositable
              # motorRPM40 ON rotationB: high-speed for 10 seconds -> ensures object reaches end of conveyer belt
    
        
