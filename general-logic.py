# RUN THIS PROGRAM ON THE TERMINAL, NOT ON IDE.
    # Ensure circuitry is correctly connected
    # Place all the .py files under the same directory level
    # Create a 'pics' folder in that directory
    # Open RaspPi's Terminal & cd into the .py files' directory
    # Enter: python3 general-logic.py


# The .py files used on specific components
from ultrasonic import *  # .py code to measures object's distance from sensor 
from cam import *  # Webcam takes photo, converts 400x300 resolution, saves in "pics" directory
from rekog import *
from motorsDC import * # Code to operate the RPM20 & RPM40 DC motors

booleanIsCompostable = 1 # 1 = compositable, 0 = non-compositable
objectNumber = 0

try:
    print("[INFO]: Hello! Press 'r' to begin. Press 'Ctrl+C' to terminate program, do NOT press the IDE's red button.")

    while(1):
         x = input()
         
         motor_OFF('rpm20')
         motor_OFF('rpm40')
    
         if x=='r':
            print("\n[INFO]: Pressed 'r'... Starting program!\n")
            
            while True:
                distanceOfObject = measure_object_distance()

                if (distanceOfObject < 30.0):
                    objectNumber += 1
                    motor_adjust_speed('rpm20', 0)
                    
                    print("[INFO]: Object #%d is %.1f cm within boundary. Taking picture..." % (objectNumber, distanceOfObject))
                    startTime = time.perf_counter()
                    file_path = process_webcam_photo(objectNumber)
                    booleanIsCompostable = predict(file_path)
                    endTime = time.perf_counter()
                    print("[INFO]: Finished processing photo in %.1f seconds!\n" % (endTime - startTime) )

                    if (booleanIsCompostable == 1):
                        print("[INFO]: Object is compostable. Sending to compost bin...")
                    else:
                        print("[INFO]: Object is not compostable. Sending to trash bin...")
                    send_to_second_conveyor_belt()
                    
                    print("[INFO]: Object is moving towards the bin...")
                    send_to_bin(booleanIsCompostable)
                    print("[INFO]: Object has been deposited into the bin! Ready to process the next object!\n")

                else:
                    motor_adjust_speed('rpm20', 100)
                    motor_adjust_speed('rpm40', 0)
                    
                time.sleep(0.5)
                    
except KeyboardInterrupt:
    print("[INFO]: Pressed 'Ctrl+C'... Terminating program!")

finally:
    motor_OFF('rpm20')
    motor_OFF('rpm40')
    GPIO.cleanup()
    time.sleep(0.8)
    print("[INFO]: System Terminated!")

                
