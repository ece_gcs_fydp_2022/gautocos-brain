import cv2
import os

def process_webcam_photo(numPic):
    
    cam = cv2.VideoCapture(0)
    ret, frame = cam.read()
    resizeFrame = cv2.resize(frame, (640, 640), interpolation = cv2.INTER_AREA)
    img_path = ""
    if not ret:
        print("[ERROR]: Failed to get frame")
    
    else:
        path = './pics/'
        img_name = "Object{}-Photo.jpg".format(numPic)
        img_path = path + img_name
        isWritten = cv2.imwrite(os.path.join(path , img_name), resizeFrame)
        print("[INFO]: {} created!".format(img_name))
#         print("[DEBUG]: Cam resolution: %d x %d" % (resizeFrame.shape[1], resizeFrame.shape[0]))

    cam.release()
    cv2.destroyAllWindows()
    return img_path